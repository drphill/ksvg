/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

plugins {
    // Apply the org.jetbrains.kotlin.jvm Plugin to add support for Kotlin.
    id("org.jetbrains.kotlin.jvm") version "1.5.31"

    // Apply the java-library plugin for API and implementation separation.
    `java-library`
    `maven-publish`
    `application`

    // https://plugins.gradle.org/plugin/org.jlleitschuh.gradle.ktlint
    id("org.jlleitschuh.gradle.ktlint") version "10.1.0"

    // https://github.com/java9-modularity/gradle-modules-plugin/blob/master/test-project-kotlin/build.gradle.kts
    id("org.javamodularity.moduleplugin") version "1.8.9"

    //  id("io.gitlab.arturbosch.detekt").version("1.19.0")
}

repositories {
    // Use Maven Central for resolving dependencies.
    mavenCentral()
    mavenLocal()
}

dependencies {
    // Align versions of all Kotlin components
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    // Use the Kotlin JDK 8 standard library.
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

//    // used for escaping html/xml strings
//    implementation("org.apache.commons:commons-text:1.9")

    testImplementation(kotlin("test"))

    implementation("org.apache.logging.log4j:log4j-api-kotlin:1.0.0")
    implementation("org.apache.logging.log4j:log4j-api:2.11.1")
    implementation("org.apache.logging.log4j:log4j-core:2.11.1")

    // implementation("uk.vanleersum:diagram:0.0.2")
    implementation(project(":ksvg"))
}

application {
    // Define the main class for the application.
    mainClass.set("uk.vanleersum.svgapp.EntryPoint")
    mainModule.set("uk.vanleersum.svgapp")

    // https://bugs.openjdk.java.net/browse/JDK-8211302
    when {
        org.gradle.internal.os.OperatingSystem.current().isLinux ->
            applicationDefaultJvmArgs = listOf("-Djdk.gtk.version=2")
    }
}

// jlink {
//    addOptions("--strip-debug", "--compress", "2", "--no-header-files", "--no-man-pages")
//    mergedModule {
//        additive = true
//        requires("org.apache.logging.log4j")
//    }
//    forceMerge("kotlin")
//    forceMerge("slf4j")
//
//    imageName.set("StateTransitionModeller-$discriminator-$version")
//    println("1 " + imageName)
//
//    launcher {
//        name = "StateTransitionModeller-$discriminator"
//        noConsole = true
//        if (org.gradle.internal.os.OperatingSystem.current().isMacOsX) {
//            name = "$name.command"
//        } else if (org.gradle.internal.os.OperatingSystem.current().isLinux) {
//            name = "$name.sh"
//        }
//    }
//
//    jpackage {
//        imageName = "StateTransitionModeller-$discriminator"
//        skipInstaller = false
//        installerName = "StateTransitionModeller-$discriminator"
//        when {
//            org.gradle.internal.os.OperatingSystem.current().isWindows -> {
//                installerOptions =
//                    listOf("--win-menu", "--win-shortcut")
//            }
//            org.gradle.internal.os.OperatingSystem.current().isMacOsX -> {
//                appVersion = "1.0.0"
//            }
//        }
//    }
// }
