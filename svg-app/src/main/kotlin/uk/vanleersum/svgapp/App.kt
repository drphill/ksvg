/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */
package uk.vanleersum.svgapp

import org.apache.logging.log4j.kotlin.Logging
import org.xml.sax.InputSource
import org.xml.sax.XMLReader
import uk.vanleersum.ksvg.parser.SvgParser
import uk.vanleersum.ksvg.parser.TagWalker
import uk.vanleersum.ksvg.svg.SvgElement
import java.awt.BorderLayout
import java.awt.FlowLayout
import java.awt.Graphics
import java.awt.Graphics2D
import java.io.File
import java.io.StringReader
import javax.swing.JButton
import javax.swing.JFileChooser
import javax.swing.JFrame
import javax.swing.JMenu
import javax.swing.JMenuBar
import javax.swing.JMenuItem
import javax.swing.JPanel
import javax.swing.JScrollPane
import javax.swing.JSplitPane
import javax.swing.JTextArea
import javax.swing.filechooser.FileNameExtensionFilter
import javax.xml.parsers.SAXParser
import javax.xml.parsers.SAXParserFactory

internal class App : JFrame("SVG App"), Logging {
    private var svgElement: SvgElement? = null
    private val panel = object : JPanel() {
        override fun paint(g: Graphics) {
            super.paint(g)
            if (g !is Graphics2D) return
            svgElement?.let {
                it.render(g)
            }
        }
    }

    private val fc = JFileChooser(File("."))
    private val xmlTextArea = JTextArea()

    init {
        val borderPane = JPanel(BorderLayout())
        borderPane.add(JScrollPane(xmlTextArea), BorderLayout.CENTER)
        val button = JButton("Render").apply {
            addActionListener { renderText() }
        }
        val buttonBar = JPanel(FlowLayout())
        buttonBar.add(button)
        borderPane.add(buttonBar, BorderLayout.SOUTH)
        val splitPane = JSplitPane(
            JSplitPane.HORIZONTAL_SPLIT,
            borderPane,
            panel
        )

        contentPane.add(splitPane, BorderLayout.CENTER)

        val menuBar = JMenuBar()
        val fileMenu = JMenu("File")
        menuBar.add(fileMenu)
        JMenuItem("Load").apply {
            addActionListener { load() }
            fileMenu.add(this)
        }
        JMenuItem("Save").apply {
            addActionListener { save() }
            fileMenu.add(this)
        }
        fileMenu.add(
            Examples.menu {
                xmlTextArea.text = it
                renderText()
            }
        )
        JMenuItem("Exit").apply {
            addActionListener { System.exit(0) }
            fileMenu.add(this)
        }
        contentPane.add(menuBar, BorderLayout.NORTH)
//        // panel.revalidate()
//
//        addComponentListener(object : ComponentAdapter() {
//            override fun componentShown(e: ComponentEvent?) {
//                viewer.redraw()
//            }
//        })
        logger.debug { "Debugging" }
    }

    private fun load() {
        logger.debug { "load" }
        fc.isAcceptAllFileFilterUsed = false
        fc.fileFilter = FileNameExtensionFilter("SVG Images", "svg")
        if (JFileChooser.APPROVE_OPTION != this.fc.showOpenDialog(this)) return
        val file = fc.selectedFile
        xmlTextArea.text = file.readText()
        renderText()
    }

    private fun save() {
        if (JFileChooser.APPROVE_OPTION != fc.showSaveDialog(this)) return

        var file = fc.selectedFile
        logger.debug { file.path }
        if (!file.name.endsWith(".svg")) {
            file = File(file.path + ".svg")
        }
        file.writeText(xmlTextArea.text)
    }

    private fun renderText() {
        val xml = xmlTextArea.text
        val spf = SAXParserFactory.newInstance()
        spf.isNamespaceAware = true
        val saxParser: SAXParser = spf.newSAXParser()
        val xmlReader: XMLReader = saxParser.xmlReader
        val svgParser = uk.vanleersum.ksvg.parser.SvgParser()
        xmlReader.contentHandler = svgParser
        val src = InputSource(StringReader(xml))
        xmlReader.parse(src)
        svgParser.root?.let {
            val svg = TagWalker(it).walk()
            svgElement = svg
            panel.repaint()
        }
    }
}
