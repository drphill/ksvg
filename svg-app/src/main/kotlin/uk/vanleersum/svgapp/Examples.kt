package uk.vanleersum.svgapp
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import javax.swing.JMenu
import javax.swing.JMenuItem

object Examples {
    val basicShapes = mapOf(
        "rectangle" to """
            <svg width="888" height="579" xmlns="http://www.w3.org/2000/svg">
                <g transform="translate(215,71)">
                    <rect width="170" height="58" rx="29" ry="29"  
                        stroke="#0080ff"  fill="#ffffff"  
                        stroke-dasharray="10,5,10"
                        stroke-width="4"/>
                </g>
            </svg>
        """.trimIndent(),
        "circle" to """
            <svg width="888" height="579" xmlns="http://www.w3.org/2000/svg">
                <circle cx="200" cy="200" r="200"
                        fill="red" stroke="blue" stroke-width="10"  />
            </svg>
        """.trimIndent(),
        "ellipse" to """
            <svg width="888" height="579" xmlns="http://www.w3.org/2000/svg">
                <ellipse cx="250" cy="100" rx="250" ry="100"
                    fill="none" stroke="blue" stroke-width="20"  />
            </svg>
        """.trimIndent(),
        "polygon" to """
            <svg width="12cm" height="4cm" viewBox="0 0 1200 400"
                 xmlns="http://www.w3.org/2000/svg" version="1.2" baseProfile="tiny">
              <polygon fill="lime" stroke="blue" stroke-width="2" 
                        points="850,75  958,137.5 958,262.5
                                850,325 742,262.6 742,137.5" />
            </svg>
        """.trimIndent(),
        "polygon2" to """
            <svg width="12cm" height="4cm" viewBox="0 0 1200 400"
                 xmlns="http://www.w3.org/2000/svg" version="1.2" baseProfile="tiny">
              <polygon fill="red" stroke="blue" stroke-width="2" 
                        points="350,75  379,161 469,161 397,215
                                423,301 350,250 277,301 303,215
                                231,161 321,161" />
              <polygon fill="lime" stroke="blue" stroke-width="2" 
                        points="850,75  958,137.5 958,262.5
                                850,325 742,262.6 742,137.5" />
            </svg>
        """.trimIndent(),
        "polyline" to """
        <svg width="12cm" height="4cm" viewBox="0 0 1200 400"
               xmlns="http://www.w3.org/2000/svg">
            <polyline fill="none" stroke="blue" stroke-width="10" stroke-linecap="round"
            points="50,375
                    150,375 150,325 250,325 250,375
                    350,375 350,250 450,250 450,375
                    550,375 550,175 650,175 650,375
                    750,375 750,100 850,100 850,375
                    950,375 950,25 1050,25 1050,375
                    1150,375" />
         </svg>
        """.trimIndent(),
        "lines" to """
            <svg width="12cm" height="4cm" viewBox="0 0 1200 400"
                 xmlns="http://www.w3.org/2000/svg">
              <g stroke="green" >
                <line x1="100" y1="300" x2="300" y2="100"
                        stroke-width="5"  />
                <line x1="300" y1="300" x2="500" y2="100"
                        stroke-width="10"  />
                <line x1="500" y1="300" x2="700" y2="100"
                        stroke-width="15"  />
                <line x1="700" y1="300" x2="900" y2="100"
                        stroke-width="20"  />
                <line x1="900" y1="300" x2="1100" y2="100"
                        stroke-width="25"  />
              </g>
            </svg>
        """.trimIndent(),
        "path" to """
            <svg width="888" height="579" xmlns="http://www.w3.org/2000/svg">   
            <path d="M 100 100 L 300 100 L 200 300 z"
                    fill="red" stroke="blue" stroke-width="3" />  
            </svg>
        """.trimIndent()
    )

    val curves = mapOf(
        "1" to """
            <svg width="888" height="579" xmlns="http://www.w3.org/2000/svg">   
            <path d="M100,200 C100,100 250,100 250,200
                                       S400,300 400,200"
                    fill="red" stroke="blue" stroke-width="3" />  
            </svg>
        """.trimIndent(),
        "1" to """
            <svg width="888" height="579" xmlns="http://www.w3.org/2000/svg">   
            <path d="M100,200 C100,100 250,100 250,200
                                       S400,300 400,200"
                    fill="red" stroke="blue" stroke-width="3" />  
            </svg>
        """.trimIndent(),
        "2" to """
            <svg width="888" height="579" xmlns="http://www.w3.org/2000/svg">   
            <path d="M200,300 L400,50 L600,300 L800,550 L1000,300"
                    fill="red" stroke="blue" stroke-width="3" />  
            </svg>
        """.trimIndent(),
        "3" to """
            <svg width="888" height="579" xmlns="http://www.w3.org/2000/svg">   
            <path d="M300,200 h-150 a150,150 0 1,0 150,-150 z"
                fill="red" stroke="blue" stroke-width="5" />
          <path d="M275,175 v-150 a150,150 0 0,0 -150,150 z"
                fill="yellow" stroke="blue" stroke-width="5" />

            <path d="M600,350 l 50,-25 
               a25,25 -30 0,1 50,-25 l 50,-25 
               a25,50 -30 0,1 50,-25 l 50,-25 
               a25,75 -30 0,1 50,-25 l 50,-25 
               a25,100 -30 0,1 50,-25 l 50,-25"
                fill="none" stroke="red" stroke-width="5"  /> 
            </svg>
        """.trimIndent(),
    )

    val all = mapOf(
        "basic shapes" to basicShapes,
        "curves" to curves
    )

    fun menu(action: (String) -> Unit) = JMenu("Example").also { menu ->
        all.entries.forEach { (name, sublist) ->
            JMenu(name).let { subMenu ->
                sublist.entries.forEach { (name, xml) ->
                    JMenuItem(name).apply {
                        addActionListener { action(xml) }
                        subMenu.add(this)
                    }
                }
                menu.add(subMenu)
            }
        }
    }
}
