package uk.vanleersum.svgapp.parser

import org.junit.jupiter.api.Test
import org.xml.sax.InputSource
import org.xml.sax.XMLReader
import java.io.StringReader
import javax.xml.parsers.SAXParser
import javax.xml.parsers.SAXParserFactory
import kotlin.test.assertEquals

internal class SvgParserTest {
    @Test
    fun testParser() {
        val xml = """
            <?xml version="1.0" encoding="UTF-8" standalone="no"?>
            <svg width="888" height="579" xmlns="http://www.w3.org/2000/svg">
                <rect fill="#f0f0f0" stroke="#000" width="888" height="579"/>
                <g transform="translate(215,71)">
                    <rect width="170" height="58"  
                          rx="29" ry="29"  fill="#ffffff"  
                          stroke-width="1" stroke="#000000"/>
                </g>
            </svg>
        """.trimIndent()

        val spf = SAXParserFactory.newInstance()
        spf.isNamespaceAware = true
        val saxParser: SAXParser = spf.newSAXParser()
        val xmlReader: XMLReader = saxParser.xmlReader
        val svgParser = uk.vanleersum.ksvg.parser.SvgParser()
        xmlReader.contentHandler = svgParser

        val src = InputSource(StringReader(xml))
        xmlReader.parse(src)

        val root = svgParser.root
        requireNotNull(root)
        val flat = root.flattened
        assertEquals(4, flat.size)
    }
}
