package uk.vanleersum.ksvg.path

import org.apache.logging.log4j.kotlin.Logging
import kotlin.test.Test
import kotlin.test.assertEquals

internal class PathTokeniserTest : Logging {

    @Test
    internal fun test1() {
        val tokens = PathTokeniser.tokenise("M275,175 v-150 a150,150 0 0,0 -150,150 z")
        logger.debug { tokens.joinToString { it.toString() } }
        assertEquals(14, tokens.size)
    }
    @Test
    internal fun test2() {
        val tokens = PathTokeniser.tokenise("M 275 175 v -150 a 150 150 0 0 0 -150 150 z")
        logger.debug { tokens.joinToString { it.toString() } }
        assertEquals(14, tokens.size)
    }
    @Test
    internal fun test3() {
        val tokens = PathTokeniser.tokenise("M275,175v-150a150,150,0,0,0,-150,150z")
        logger.debug { tokens.joinToString { it.toString() } }
        assertEquals(14, tokens.size)
    }

    // M13.79,7a9.32,9.32,0,0,1,1.08-.64c.23-.13.39-.07.44-.28

    @Test
    internal fun test4() {
        // aggressive compaction here causes confusion
        val tokens = PathTokeniser.tokenise("M 275 175 c .23 -.13 .39 -.07 .44 -.28 z")
        logger.debug { tokens.joinToString { it.toString() } }
        assertEquals(11, tokens.size)

        val tokens2 = PathTokeniser.tokenise("M275,175c.23-.13.39-.07.44-.28z")
        logger.debug { tokens2.joinToString { it.toString() } }
        assertEquals(11, tokens2.size)
    }

    @Test
    internal fun test5() {
        // aggressive compaction here causes confusion
        val tokens = PathTokeniser.tokenise("M100,200 C100,100 250,100 250,200 S400,300 400,200")
        logger.debug { tokens.joinToString { it.toString() } }
        assertEquals(11, tokens.size)

        val tokens2 = PathTokeniser.tokenise("M100,200 C100,100 250,100 250,200 S400,300 400,200")
        logger.debug { tokens2.joinToString { it.toString() } }
        assertEquals(11, tokens2.size)
    }
}
