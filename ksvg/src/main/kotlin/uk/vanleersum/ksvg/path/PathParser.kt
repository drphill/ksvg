package uk.vanleersum.ksvg.path
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import org.apache.logging.log4j.kotlin.Logging
import java.awt.geom.AffineTransform
import java.awt.geom.Arc2D
import java.awt.geom.GeneralPath

internal class PathParser(val pathString: String, windingRule: Int) : Logging {

    internal val path = GeneralPath(windingRule)

    internal fun parse() {
        logger.debug { pathString }
        val tokens = PathTokeniser.tokenise(pathString)
        logger.debug { tokens.joinToString { it.toString() } }
        val commands = CommandExtractor.extractCommands(tokens).toMutableList()
        logger.debug { commands.joinToString("\n\t", "\n\t") { it.toString() } }
        var command = commands.removeFirstOrNull()
        while (null != command) {
            logger.debug { command }
            when (command.type) {
                PathCommandType.CubicToToAbsolute -> cubic(command)
                PathCommandType.CubicToRelative -> cubic(command)
                PathCommandType.CubicSmoothToToAbsolute -> cubicSmooth(command)
                PathCommandType.CubicSmoothToRelative -> cubicSmooth(command)
                PathCommandType.EllipticToToAbsolute -> arc(command)
                PathCommandType.EllipticToRelative -> arc(command)
                PathCommandType.HorizontalToToAbsolute -> horizontal(command)
                PathCommandType.HorizontalToRelative -> horizontal(command)
                PathCommandType.LineToAbsolute -> line(command)
                PathCommandType.LineToRelative -> line(command)
                PathCommandType.MoveToAbsolute -> move(command)
                PathCommandType.MoveToRelative -> move(command)
                PathCommandType.QuadraticCurveToToAbsolute -> quadratic(command)
                PathCommandType.QuadraticCurveToRelative -> quadratic(command)
                PathCommandType.QuadraticSmoothCurveToToAbsolute -> quadraticSmooth(command)
                PathCommandType.QuadraticSmoothCurveToRelative -> quadraticSmooth(command)
                PathCommandType.VerticalToAbsolute -> vertical(command)
                PathCommandType.VerticalToRelative -> vertical(command)
                PathCommandType.ClosePathAbsolute -> closePath()
                PathCommandType.ClosePathRelative -> closePath()
            }
            command = commands.removeFirstOrNull()
        }
    }

    // restart flag set when the very next MoveTo should define a new start
    // position.  Flag should be cleared by all commands
    private var restart = true
    private var startX = 0f
    private var startY = 0f
    private var lastX = 0f
    private var lastY = 0f

    // to draw smooth cubic and quadratics we need to remember the previous command's
    // second control point (if the previous command had control points).
    // The value defaults to the previous command's last point
    private var controlX = 0f
    private var controlY = 0f

    private fun arc(command: PathCommand) {
        val dx = if (command.type.relative) lastX else 0f
        val dy = if (command.type.relative) lastY else 0f
        val rx = command.floats[0]
        val ry = command.floats[1]
        val xRotation = command.floats[2]
        val largeArc = command.floats[3] != 0f
        val sweep = command.floats[4] != 0f
        val x = command.floats[5] + dx
        val y = command.floats[6] + dy
        arcTo(path, rx, ry, xRotation, largeArc, sweep, x, y, lastX, lastY)
        lastX = x
        lastY = y
        controlX = x
        controlY = y
        restart = false
    }

    /**
     * Adds an elliptical arc, defined by two radii, an angle from the
     * x-axis, a flag to choose the large arc or not, a flag to
     * indicate if we increase or decrease the angles and the final
     * point of the arc.
     *
     * @param path The path that the arc will be appended to.
     *
     * @param rx the x radius of the ellipse
     * @param ry the y radius of the ellipse
     *
     * @param angle the angle from the x-axis of the current
     * coordinate system to the x-axis of the ellipse in degrees.
     *
     * @param largeArcFlag the large arc flag. If true the arc
     * spanning less than or equal to 180 degrees is chosen, otherwise
     * the arc spanning greater than 180 degrees is chosen
     *
     * @param sweepFlag the sweep flag. If true the line joining
     * center to arc sweeps through decreasing angles otherwise it
     * sweeps through increasing angles
     *
     * @param x the absolute x coordinate of the final point of the arc.
     * @param y the absolute y coordinate of the final point of the arc.
     * @param x0 - The absolute x coordinate of the initial point of the arc.
     * @param y0 - The absolute y coordinate of the initial point of the arc.
     */
    fun arcTo(
        path: GeneralPath,
        rx: Float,
        ry: Float,
        angle: Float,
        largeArcFlag: Boolean,
        sweepFlag: Boolean,
        x: Float,
        y: Float,
        x0: Float,
        y0: Float
    ) {

        // Ensure radii are valid
        if (rx == 0f || ry == 0f) {
            path.lineTo(x, y)
            return
        }
        if (x0 == x && y0 == y) {
            // If the endpoints (x, y) and (x0, y0) are identical, then this
            // is equivalent to omitting the elliptical arc segment entirely.
            return
        }
        val arc: Arc2D = ArcCalculation.computeArc(
            x0.toDouble(), y0.toDouble(), rx.toDouble(), ry.toDouble(), angle.toDouble(),
            largeArcFlag, sweepFlag, x.toDouble(), y.toDouble()
        )
        val t = AffineTransform.getRotateInstance(Math.toRadians(angle.toDouble()), arc.centerX, arc.centerY)
        val s = t.createTransformedShape(arc)
        path.append(s, true)
    }

    private fun closePath() {
        // https://www.w3.org/TR/2011/REC-SVG11-20110816/paths.html#PathDataClosePathCommand
        path.closePath()
        restart = true
    }

    private fun cubic(command: PathCommand) {
        val dx = if (command.type.relative) lastX else 0f
        val dy = if (command.type.relative) lastY else 0f
        val control1x = command.floats[0] + dx
        val control1y = command.floats[1] + dy
        val control2x = command.floats[2] + dx
        val control2y = command.floats[3] + dy
        val x = command.floats[4] + dx
        val y = command.floats[5] + dy
        path.curveTo(control1x, control1y, control2x, control2y, x, y)
        lastY = y
        lastX = x
        controlX = control2x
        controlY = control2y
    }

    private fun cubicSmooth(command: PathCommand) {
        // https://www.w3.org/TR/2011/REC-SVG11-20110816/paths.html#PathDataCubicBezierCommands
        val dx = if (command.type.relative) lastX else 0f
        val dy = if (command.type.relative) lastY else 0f
        val control1x = lastX * 2 - controlX
        val control1y = lastY * 2 - controlY
        val control2x = command.floats[0] + dx
        val control2y = command.floats[1] + dy
        val x = command.floats[2] + dx
        val y = command.floats[3] + dy
        path.curveTo(control1x, control1y, control2x, control2y, x, y)
        lastY = y
        lastX = x
        controlX = control2x
        controlY = control2y
    }

    private fun horizontal(command: PathCommand) {
        // https://www.w3.org/TR/2011/REC-SVG11-20110816/paths.html#PathDataLinetoCommands
        var x = command.floats[0]
        if (command.type.relative) {
            x += lastX
        }
        path.lineTo(x, lastY)
        lastX = x
        restart = false
        controlX = x
    }

    private fun line(command: PathCommand) {
        // https://www.w3.org/TR/2011/REC-SVG11-20110816/paths.html#PathDataLinetoCommands
        var x = command.floats[0]
        var y = command.floats[1]
        if (command.type.relative) {
            x += lastX
            y += lastY
        }
        path.lineTo(x, y)
        lastX = x
        lastY = y
        controlX = x
        controlY = y
        restart = false
    }

    private fun move(command: PathCommand) {
        // https://www.w3.org/TR/2011/REC-SVG11-20110816/paths.html#PathDataMovetoCommands
        var x = command.floats[0]
        var y = command.floats[1]
        if (command.type.relative) {
            x += lastX
            y += lastY
        }
        path.moveTo(x, y)
        if (restart) {
            startX = x
            startY = y
        }
        lastX = x
        lastY = y
        controlX = x
        controlY = y
        restart = false
    }

    private fun vertical(command: PathCommand) {
        // https://www.w3.org/TR/2011/REC-SVG11-20110816/paths.html#PathDataLinetoCommands
        var y = command.floats[0]
        if (command.type.relative) {
            y += lastY
        }
        path.lineTo(lastX, y)
        lastY = y
        controlY = y
        restart = false
    }

    private fun quadratic(command: PathCommand) {
        val dx = if (command.type.relative) lastX else 0f
        val dy = if (command.type.relative) lastY else 0f
        val control1x = command.floats[0] + dx
        val control1y = command.floats[1] + dy
        val x = command.floats[2] + dx
        val y = command.floats[3] + dy
        path.quadTo(control1x, control1y, x, y)
    }

    private fun quadraticSmooth(command: PathCommand) {
        val dx = if (command.type.relative) lastX else 0f
        val dy = if (command.type.relative) lastY else 0f
        val control1x = lastX * 2 - controlX
        val control1y = lastY * 2 - controlY
        val x = command.floats[0] + dx
        val y = command.floats[1] + dy
        path.quadTo(control1x, control1y, x, y)
    }
}
