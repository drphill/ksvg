package uk.vanleersum.ksvg.path
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

// https://www.w3.org/TR/2011/REC-SVG11-20110816/paths.html
internal enum class PathCommandType(
    val string: String,
    val count: Int,
    val relative: Boolean,
    val repeat: Boolean
) {
    EllipticToToAbsolute("A", 7, false, true),
    EllipticToRelative("a", 7, true, true),
    CubicToToAbsolute("C", 6, false, true),
    CubicToRelative("c", 6, true, true),
    HorizontalToToAbsolute("H", 1, false, true),
    HorizontalToRelative("h", 1, true, true),
    LineToAbsolute("L", 2, false, true),
    LineToRelative("l", 2, true, true),
    MoveToAbsolute("M", 2, false, true), // subsequent pairs are LineToAbsolute
    MoveToRelative("m", 2, true, true), // subsequent pairs are LineToRelative
    QuadraticCurveToToAbsolute("Q", 4, false, true),
    QuadraticCurveToRelative("q", 4, true, true),
    CubicSmoothToToAbsolute("S", 4, false, true),
    CubicSmoothToRelative("s", 4, true, true),
    QuadraticSmoothCurveToToAbsolute("T", 2, false, true),
    QuadraticSmoothCurveToRelative("t", 2, true, true),
    VerticalToAbsolute("V", 1, false, true),
    VerticalToRelative("v", 1, true, true),
    ClosePathAbsolute("Z", 0, false, false),
    ClosePathRelative("z", 0, true, false);

    companion object {
        fun forString(str: String): PathCommandType? {
            return values().firstOrNull { it.string == str }
        }
    }
}
