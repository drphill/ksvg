package uk.vanleersum.ksvg.path
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

internal object CommandExtractor {
    // Note that we may need to account for the shennanigans here:
    // https://www.w3.org/TR/2011/REC-SVG11-20110816/paths.html#PathDataClosePathCommand

    internal fun extractCommands(tokens: List<Any>): List<PathCommand> {
        // a list of the tokens converted to either a float or a commandType
        val objects: MutableList<Any> = tokens.toMutableList()
        val commands = mutableListOf<PathCommand>()
        var previousCommandType: PathCommandType? = null
        do {
            val command = extractCommand(previousCommandType, objects)
            if (null != command) {
                commands.add(command)
                // https://www.w3.org/TR/2011/REC-SVG11-20110816/paths.html#PathDataMovetoCommands

                previousCommandType = if (!command.type.repeat) {
                    null
                } else when (command.type) {
                    PathCommandType.MoveToAbsolute -> PathCommandType.LineToAbsolute
                    PathCommandType.MoveToRelative -> PathCommandType.LineToRelative
                    else -> command.type
                }
            }
        } while (null != command)
        return commands.toList()
    }

    // return a command if one can be made.  Note that the command may not
    // have the required number of floats.....
    private fun extractCommand(
        previousCommandType: PathCommandType?,
        objects: MutableList<Any>
    ): PathCommand? {
        val commandType = extractCommandType(previousCommandType, objects) ?: return null
        val floats = extractFloats(commandType.count, objects)
        return PathCommand(commandType, floats)
    }

    private fun extractFloats(
        count: Int,
        objects: MutableList<Any>
    ): FloatArray {
        val floats = mutableListOf<Float>()
        var broken = false
        while (floats.size < count && !broken) {
            val obj = objects.firstOrNull() as? Float
            if (null == obj) {
                broken = true
            } else {
                floats.add(obj)
                objects.removeFirst()
            }
        }
        return floats.toFloatArray()
    }

    // if we have a command type next in the stream then use it
    // if we do not have a previous type
    private fun extractCommandType(previousCommandType: PathCommandType?, objects: MutableList<Any>): PathCommandType? {
        val next = objects.firstOrNull() ?: return null
        return when {
            next is PathCommandType -> {
                objects.removeFirst()
                next
            }
            null == previousCommandType -> return null
            else -> previousCommandType
        }
    }
}
