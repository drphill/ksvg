package uk.vanleersum.ksvg.path

import java.awt.geom.Arc2D
import kotlin.math.abs
import kotlin.math.acos
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt

/**
 * Adapted from svgSalamander project
 */
internal object ArcCalculation {
    /**
     * This constructs an unrotated Arc2D from the SVG specification of an
     * Elliptical arc.  To get the final arc you need to apply a rotation
     * transform such as:
     *
     * AffineTransform.getRotateInstance
     * (angle, arc.getX()+arc.getWidth()/2, arc.getY()+arc.getHeight()/2);
     *
     * @param x0 origin of arc in x
     * @param y0 origin of arc in y
     * @param rx radius of arc in x
     * @param ry radius of arc in y
     * @param angle number of radians in arc
     * @param largeArcFlag
     * @param sweepFlag
     * @param x ending coordinate of arc in x
     * @param y ending coordinate of arc in y
     * @return arc shape
     */
    fun computeArc(
        x0: Double,
        y0: Double,
        rx: Double,
        ry: Double,
        angle: Double,
        largeArcFlag: Boolean,
        sweepFlag: Boolean,
        x: Double,
        y: Double
    ): Arc2D {
        //
        // Elliptical arc implementation based on the SVG specification notes
        //

        // Compute the half distance between the current and the final point
        var vrx = rx
        var vry = ry
        var vangle = angle
        val dx2 = (x0 - x) / 2.0
        val dy2 = (y0 - y) / 2.0
        // Convert angle from degrees to radians
        vangle = Math.toRadians(vangle % 360.0)
        val cosAngle = cos(vangle)
        val sinAngle = sin(vangle)

        //
        // Step 1 : Compute (x1, y1)
        //
        val x1 = cosAngle * dx2 + sinAngle * dy2
        val y1 = -sinAngle * dx2 + cosAngle * dy2
        // Ensure radii are large enough
        vrx = abs(vrx)
        vry = abs(vry)
        var prx = vrx * vrx
        var pry = vry * vry
        val px1 = x1 * x1
        val py1 = y1 * y1
        // check that radii are large enough
        val radiiCheck = px1 / prx + py1 / pry
        if (radiiCheck > 1) {
            vrx *= sqrt(radiiCheck)
            vry *= sqrt(radiiCheck)
            prx = vrx * vrx
            pry = vry * vry
        }

        //
        // Step 2 : Compute (cx1, cy1)
        //
        var sign: Double = if (largeArcFlag == sweepFlag) -1.0 else 1.0
        var sq = (prx * pry - prx * py1 - pry * px1) / (prx * py1 + pry * px1)
        sq = if (sq < 0) 0.0 else sq
        val coef = sign * sqrt(sq)
        val cx1 = coef * (vrx * y1 / vry)
        val cy1 = coef * -(vry * x1 / vrx)

        //
        // Step 3 : Compute (cx, cy) from (cx1, cy1)
        //
        val sx2 = (x0 + x) / 2.0
        val sy2 = (y0 + y) / 2.0
        val cx = sx2 + (cosAngle * cx1 - sinAngle * cy1)
        val cy = sy2 + (sinAngle * cx1 + cosAngle * cy1)

        //
        // Step 4 : Compute the angleStart (angle1) and the angleExtent (dangle)
        //
        val ux = (x1 - cx1) / vrx
        val uy = (y1 - cy1) / vry
        val vx = (-x1 - cx1) / vrx
        val vy = (-y1 - cy1) / vry
        // Compute the angle start
        var n: Double = sqrt(ux * ux + uy * uy)
        var p: Double = ux // (1 * ux) + (0 * uy)
        sign = if (uy < 0) -1.0 else 1.0
        var angleStart = Math.toDegrees(sign * acos(p / n))

        // Compute the angle extent
        n = sqrt((ux * ux + uy * uy) * (vx * vx + vy * vy))
        p = ux * vx + uy * vy
        sign = if (ux * vy - uy * vx < 0) -1.0 else 1.0
        var angleExtent = Math.toDegrees(sign * acos(p / n))
        if (!sweepFlag && angleExtent > 0) {
            angleExtent -= 360.0
        } else if (sweepFlag && angleExtent < 0) {
            angleExtent += 360.0
        }
        angleExtent %= 360.0
        angleStart %= 360.0

        //
        // We can now build the resulting Arc2D in double precision
        //
        val arc = Arc2D.Double()
        arc.x = cx - vrx
        arc.y = cy - vry
        arc.width = vrx * 2.0
        arc.height = vry * 2.0
        arc.start = -angleStart
        arc.extent = -angleExtent
        return arc
    }
}
