package uk.vanleersum.ksvg.svg
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import org.apache.logging.log4j.kotlin.Logging
import java.awt.geom.AffineTransform

object Transform : Logging {
    enum class Function(val functionName: String) {
        Translate("translate"), None("")
    }

    /**
     * Parse a string into an affine transform.
     * Note need to cope with multiple transforms:
     * https://riptutorial.com/svg/example/11163/multiple-transformations
     */
    fun parse(string: String?): AffineTransform {
        val transform = AffineTransform()
        if (null != string) {
            val function = Function.values().firstOrNull { string.contains(it.functionName) }
            if (null != function && Function.None != function) {
                val index = string.indexOf(function.functionName)
                val remainder = string.substring(index + function.functionName.length)
                // logger.debug { "Remainder = $remainder" }
                val args = remainder.split('(', ',', ')')
                    .map { it.trim() }
                    .filter { it.isNotBlank() }
                    .map { DimensionedValue.from(it) }

                when (function) {
                    Function.Translate -> {
                        if (args.size == 1) {
                            transform.setToTranslation(args[0].value, 0.0)
                            // logger.debug { "translate ${args[0].value}, 0.0" }
                        } else {
                            // logger.debug { "translate ${args[0].value}, ${args[1].value}" }
                            transform.setToTranslation(args[0].value, args[1].value)
                        }
                    }
                    else -> {}
                }
            }
        }
        return transform
    }
}
