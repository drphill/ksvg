package uk.vanleersum.ksvg.svg
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import org.apache.logging.log4j.kotlin.Logging
import java.awt.Graphics2D
import java.awt.geom.AffineTransform

/**
 * represents any element in an svg tree. specific behaviour of the
 * element is implemented by delegation
 */
class SvgElement(
    val name: String,
    val context: SvgContext
) : Logging {
    private val children = mutableListOf<SvgElement>()
    val renderable: SvgRenderable?
    val transform: AffineTransform = Transform.parse(context.attributes["transform"])

    init {
        renderable = when (name) {
            "g" -> SvgGroup(context)
            "rect" -> SvgRectangle(context)
            "circle" -> SvgCircle(context)
            "ellipse" -> SvgEllipse(context)
            "polygon" -> SvgPolygon(context)
            "polyline" -> SvgPolyline(context)
            "line" -> SvgLine(context)
            "text" -> SvgText(context)
            "path" -> SvgPath(context)
            "svg" -> null
            else -> null
        }
    }

    fun render(graphics: Graphics2D) {
        val cache = CacheGraphics(graphics)
        graphics.transform(transform)
        renderable?.render(graphics)
        children.forEach { it.render(graphics) }
        cache.restore(graphics)
    }

    fun add(name: String, attributes: Map<String, String>): SvgElement {
        return SvgElement(name, context.context(attributes)).also {
            children.add(it)
        }
    }
}
