package uk.vanleersum.ksvg.svg
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

data class DimensionedValue(val value: Double, val unit: Unit) {
    val int get() = value.toInt()
    val float get() = value.toFloat()

    enum class Unit(val string: String) {
        Pix("px"), Percent("%"), Inch("in"), Cm("cm"),
        Mm("mm"), Point("pt"), Em("em"), Ex("ex"), None(""),
    }
    companion object {
        val none = DimensionedValue(0.0, Unit.None)
        fun from(string: String?): DimensionedValue {
            if (null == string) return none
            val u = Unit.values().firstOrNull { string.contains(it.string) }
            if (null != u && Unit.None != u) {
                val index = string.indexOf(u.string)
                val ss = string.substring(0 until index).trim()
                val f = ss.toDoubleOrNull() ?: 0.0
                return DimensionedValue(f, u)
            } else {
                val f = string.toDoubleOrNull() ?: 0.0
                return DimensionedValue(f, Unit.None)
            }
//
//            val fpPat = Pattern.compile("([-+]?((\\d*\\.\\d+)|(\\d+))([eE][+-]?\\d+)?)(\\%|in|cm|mm|pt|pc|px|em|ex)?")
//            val matcher = fpPat.matcher(string)
//            if (matcher.find()) {
//                val fpString = matcher.group(1)
//                var value = 0f
//                try {
//                    value = fpString.toFloat()
//                    val unitString = matcher.group(6).lowercase()
//                    val unit = Unit.values().firstOrNull { it.string == unitString }
//                    requireNotNull(unit)
//                    //if ("%" == units) retVal /= 100f
//                    return DimensionedValue(value, unit)
//                } catch (e: Exception) {
//                }
//            } else {
//                val value = string.toIntOrNull()
//                if (null != value) {
//                    return DimensionedValue(value.toFloat(), Unit.None)
//                }
//            }
//            return none
        }
    }
}
