package uk.vanleersum.ksvg.svg
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import org.apache.logging.log4j.kotlin.Logging
import java.awt.BasicStroke
import kotlin.math.max

class SvgStroke(attributes: Map<String, String>) : Logging {
    // https://www.w3.org/TR/SVGMobile12/painting.html#StrokeProperties
    private val strokeWidth = DimensionedValue.from(attributes["stroke-width"])
    private val strokeMiterLimit = DimensionedValue.from(attributes["stroke-miterlimit"])
    private val dashPhase = DimensionedValue.from(attributes["stroke-dashoffse"]).float
    private val strokeDashArray = attributes["stroke-dasharray"]?.let {
        it.split(',')
            .map { it.trim() }
            .filter { it.isNotBlank() }
            .mapNotNull { it.toFloatOrNull() }
            .toFloatArray()
    }
    private val strokeCap = when (val v = attributes["stroke-linecap"]) {
        "butt" -> BasicStroke.CAP_BUTT
        "round" -> BasicStroke.CAP_ROUND
        "square" -> BasicStroke.CAP_SQUARE
        null -> BasicStroke.CAP_BUTT
        else -> {
            logger.warn { "Unrecognised stroke-cap '$v'.  Using 'butt" }
            BasicStroke.CAP_BUTT
        }
    }

    private val strokeJoin = when (val v = attributes["stroke-linejoin"]) {
        "miter" -> BasicStroke.JOIN_MITER
        "round" -> BasicStroke.JOIN_ROUND
        "bevel" -> BasicStroke.JOIN_BEVEL
        null -> BasicStroke.JOIN_MITER
        else -> {
            logger.warn { "Unrecognised stroke-join '$v'.  Using 'miter" }
            BasicStroke.JOIN_MITER
        }
    }

    val basicStroke get() = BasicStroke(
        strokeWidth.float,
        strokeCap, strokeJoin,
        max(strokeMiterLimit.float, 1f),
        strokeDashArray, dashPhase
    )
}
