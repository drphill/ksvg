package uk.vanleersum.ksvg.svg
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import org.apache.logging.log4j.kotlin.Logging
import java.awt.AlphaComposite
import java.awt.Color
import java.awt.Graphics2D
import java.awt.Point

/**
 * A representation of the current context for resolving attributes.
 */
class SvgContext(
    val attributes: Map<String, String>,
    val parent: SvgContext? = null
) : Logging {

    init {
//        attributes["fill-opacity"]?.let {
//            logger.debug { "fill-opacity = $it" }
//        }
    }

    fun context(attributes: Map<String, String>): SvgContext {
        return SvgContext(attributes, this)
    }

    // https://www.w3.org/TR/2011/REC-SVG11-20110816/masking.html#OpacityProperty
    val opacity get(): Double {
        val local = (doubleOrNull("opacity") ?: 1.0) * (parent?.opacity ?: 1.0)
        return when {
            local > 1.0 -> 1.0
            local < 0.0 -> 0.0
            else -> local
        }
    }

    // https://www.w3.org/TR/2011/REC-SVG11-20110816/painting.html#FillOpacityProperty
    val fillOpacity get(): Double {
        val local = (doubleOrNull("fill-opacity") ?: 1.0) * opacity
        return when {
            local > 1.0 -> 1.0
            local < 0.0 -> 0.0
            else -> local
        }
    }

    // https://www.w3.org/TR/2011/REC-SVG11-20110816/painting.html#StrokeOpacityProperty
    val strokeOpacity get(): Double {
        val local = (doubleOrNull("stroke-opacity") ?: 1.0) * opacity
        return when {
            local > 1.0 -> 1.0
            local < 0.0 -> 0.0
            else -> local
        }
    }

    // return the integer value for the nme (defaults to zero)
    // we will need to perform scaling conversions later
    fun integer(name: String) = DimensionedValue.from(attributes[name]).int
    // some commonly accessed integer values
    val xInt get() = integer("x")
    val yInt get() = integer("y")
    val x1Int get() = integer("x1")
    val y1Int get() = integer("y1")
    val x2Int get() = integer("x2")
    val y2Int get() = integer("y2")
    val widthInt get() = integer("width")
    val heightInt get() = integer("height")

    fun doubleOrNull(string: String) = attributes[string]?.toDoubleOrNull()

    // commonly used float values
    val radiusFloat get() = DimensionedValue.from(attributes["r"]).float
    val xRadiusFloat get() = DimensionedValue.from(attributes["rx"]).float
    val yRadiusFloat get() = DimensionedValue.from(attributes["ry"]).float
    val xCenterFloat get() = DimensionedValue.from(attributes["cx"]).float
    val yCenterFloat get() = DimensionedValue.from(attributes["cy"]).float

    val fillColour: Color? = WebColour[attributes["fill"]] ?: parent?.fillColour
    val strokeColour: Color? = WebColour[attributes["stroke"]] ?: parent?.strokeColour
    val svgStroke = SvgStroke(attributes).basicStroke

    val points
        get() = attributes["points"]?.let { str ->
            str.split(",|\\s+".toRegex())
                .map { it.toDouble().toInt() }
                .windowed(2, 2)
                .map { Point(it[0], it[1]) }
        } ?: emptyList()

    /**
     * Configure the supplied [Graphics2D] for performing a fill operation
     */
    fun setFill(graphics: Graphics2D) {
        graphics.color = fillColour
        if (fillOpacity < 1.0) {
            graphics.composite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, fillOpacity.toFloat())
        }
    }

    /**
     * Configure the supplied [Graphics2D] for performing a stroke operation
     */
    fun setStroke(graphics: Graphics2D) {
        graphics.color = strokeColour
        graphics.stroke = svgStroke
        if (strokeOpacity < 1.0) {
            graphics.composite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, strokeOpacity.toFloat())
        }
    }
}
