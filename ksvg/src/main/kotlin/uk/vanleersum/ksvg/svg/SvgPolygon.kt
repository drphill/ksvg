package uk.vanleersum.ksvg.svg
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import org.apache.logging.log4j.kotlin.Logging
import java.awt.Graphics2D
import java.awt.Polygon
import java.awt.Shape

/**
 * https://www.w3.org/TR/SVGMobile12/shapes.html#PolygonElement
 */
class SvgPolygon(val context: SvgContext) : SvgRenderable, Logging {
    override fun render(graphics: Graphics2D) {
        val points = context.points
        val shape: Shape = Polygon(
            points.map { it.x }.toIntArray(),
            points.map { it.y }.toIntArray(),
            points.size
        )
        val cache = CacheGraphics(graphics)
        context.setFill(graphics)
        graphics.fill(shape)
        cache.restore(graphics)
        context.setStroke(graphics)
        graphics.draw(shape)
        cache.restore(graphics)
    }
}
