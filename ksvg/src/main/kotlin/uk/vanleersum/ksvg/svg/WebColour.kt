package uk.vanleersum.ksvg.svg
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import org.apache.logging.log4j.kotlin.Logging
import java.awt.Color

object WebColour : Logging {
    // https://www.w3.org/TR/SVGMobile12/painting.html#colorSyntax
    // note that the table contains duplicates of colours for 'grey and 'gray'
    // https://www.december.com/html/spec/colorsvg.html
    val dict = mapOf<String, Int>(
        "aliceblue" to 0xf0f8ff,
        "antiquewhite" to 0xfaebd7,
        "aqua" to 0x00ffff,
        "aquamarine" to 0x7fffd4,
        "azure" to 0xf0ffff,
        "beige" to 0xf5f5dc,
        "bisque" to 0xffe4c4,
        "black" to 0x000000,
        "blanchedalmond" to 0xffebcd,
        "blue" to 0x0000ff,
        "blueviolet" to 0x8a2be2,
        "brown" to 0xa52a2a,
        "burlywood" to 0xdeb887,
        "cadetblue" to 0x5f9ea0,
        "chartreuse" to 0x7fff00,
        "chocolate" to 0xd2691e,
        "coral" to 0xff7f50,
        "cornflowerblue" to 0x6495ed,
        "cornsilk" to 0xfff8dc,
        "crimson" to 0xdc143c,
        "cyan" to 0x00ffff,
        "darkblue" to 0x00008b,
        "darkcyan" to 0x008b8b,
        "darkgoldenrod" to 0xb8860b,
        "darkgray" to 0xa9a9a9,
        "darkgrey" to 0xa9a9a9,
        "darkgreen" to 0x006400,
        "darkkhaki" to 0xbdb76b,
        "darkmagenta" to 0x8b008b,
        "darkolivegreen" to 0x556b2f,
        "darkorange" to 0xff8c00,
        "darkorchid" to 0x9932cc,
        "darkred" to 0x8b0000,
        "darksalmon" to 0xe9967a,
        "darkseagreen" to 0x8fbc8f,
        "darkslateblue" to 0x483d8b,
        "darkslategray" to 0x2f4f4f,
        "darkturquoise" to 0x00ced1,
        "darkviolet" to 0x9400d3,
        "deeppink" to 0xff1493,
        "deepskyblue" to 0x00bfff,
        "dimgray" to 0x696969,
        "dodgerblue" to 0x1e90ff,
        "feldspar" to 0xd19275,
        "firebrick" to 0xb22222,
        "floralwhite" to 0xfffaf0,
        "forestgreen" to 0x228b22,
        "fuchsia" to 0xff00ff,
        "gainsboro" to 0xdcdcdc,
        "ghostwhite" to 0xf8f8ff,
        "gold" to 0xffd700,
        "goldenrod" to 0xdaa520,
        "gray" to 0x808080,
        "grey" to 0x808080,
        "green" to 0x008000,
        "greenyellow" to 0xadff2f,
        "honeydew" to 0xf0fff0,
        "hotpink" to 0xff69b4,
        "indianred" to 0xcd5c5c,
        "indigo" to 0x4b0082,
        "ivory" to 0xfffff0,
        "khaki" to 0xf0e68c,
        "lavender" to 0xe6e6fa,
        "lavenderblush" to 0xfff0f5,
        "lawngreen" to 0x7cfc00,
        "lemonchiffon" to 0xfffacd,
        "lightblue" to 0xadd8e6,
        "lightcoral" to 0xf08080,
        "lightcyan" to 0xe0ffff,
        "lightgoldenrodyellow" to 0xfafad2,
        "lightgray" to 0xd3d3d3,
        "lightgrey" to 0xd3d3d3,
        "lightgreen" to 0x90ee90,
        "lightpink" to 0xffb6c1,
        "lightsalmon" to 0xffa07a,
        "lightseagreen" to 0x20b2aa,
        "lightskyblue" to 0x87cefa,
        "lightslateblue" to 0x8470ff,
        "lightslategray" to 0x778899,
        "lightsteelblue" to 0xb0c4de,
        "lightyellow" to 0xffffe0,
        "lime" to 0x00ff00,
        "limegreen" to 0x32cd32,
        "linen" to 0xfaf0e6,
        "magenta" to 0xff00ff,
        "maroon" to 0x800000,
        "mediumaquamarine" to 0x66cdaa,
        "mediumblue" to 0x0000cd,
        "mediumorchid" to 0xba55d3,
        "mediumpurple" to 0x9370d8,
        "mediumseagreen" to 0x3cb371,
        "mediumslateblue" to 0x7b68ee,
        "mediumspringgreen" to 0x00fa9a,
        "mediumturquoise" to 0x48d1cc,
        "mediumvioletred" to 0xc71585,
        "midnightblue" to 0x191970,
        "mintcream" to 0xf5fffa,
        "mistyrose" to 0xffe4e1,
        "moccasin" to 0xffe4b5,
        "navajowhite" to 0xffdead,
        "navy" to 0x000080,
        "oldlace" to 0xfdf5e6,
        "olive" to 0x808000,
        "olivedrab" to 0x6b8e23,
        "orange" to 0xffa500,
        "orangered" to 0xff4500,
        "orchid" to 0xda70d6,
        "palegoldenrod" to 0xeee8aa,
        "palegreen" to 0x98fb98,
        "paleturquoise" to 0xafeeee,
        "palevioletred" to 0xd87093,
        "papayawhip" to 0xffefd5,
        "peachpuff" to 0xffdab9,
        "peru" to 0xcd853f,
        "pink" to 0xffc0cb,
        "plum" to 0xdda0dd,
        "powderblue" to 0xb0e0e6,
        "purple" to 0x800080,
        "red" to 0xff0000,
        "rosybrown" to 0xbc8f8f,
        "royalblue" to 0x4169e1,
        "saddlebrown" to 0x8b4513,
        "salmon" to 0xfa8072,
        "sandybrown" to 0xf4a460,
        "seagreen" to 0x2e8b57,
        "seashell" to 0xfff5ee,
        "sienna" to 0xa0522d,
        "silver" to 0xc0c0c0,
        "skyblue" to 0x87ceeb,
        "slateblue" to 0x6a5acd,
        "slategray" to 0x708090,
        "snow" to 0xfffafa,
        "springgreen" to 0x00ff7f,
        "steelblue" to 0x4682b4,
        "tan" to 0xd2b48c,
        "teal" to 0x008080,
        "thistle" to 0xd8bfd8,
        "tomato" to 0xff6347,
        "turquoise" to 0x40e0d0,
        "violet" to 0xee82ee,
        "violetred" to 0xd02090,
        "wheat" to 0xf5deb3,
        "white" to 0xffffff,
        "whitesmoke" to 0xf5f5f5,
        "yellow" to 0xffff00,
        "yellowgreen" to 0x9acd32,
    )

    operator fun get(string: String?): Color? {
        if (null == string) return null
        val str = string.trim().lowercase()
        val mapped = dict[str]
        return when {
            null != mapped -> Color(mapped)
            !str.startsWith('#') -> {
                // function form
                logger.warn { "function form colour not supported yet" }
                null
            }
            str.length == 4 -> {
                // 3 digit form
                logger.warn { "3 digit colour not supported yet" }
                null
            }
            str.length == 7 -> {
                // 6 digit form
                str.substring(1)
                    .toIntOrNull(16)
                    ?.let { Color(it) }
            }
            else -> null
        }
    }
}
