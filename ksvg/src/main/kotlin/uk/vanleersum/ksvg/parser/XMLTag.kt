package uk.vanleersum.ksvg.parser
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

/**
 * Caches the values from the xml document
 */
class XMLTag private constructor(
    parentRef: XMLTag?,
    val name: String,
    attributes: Map<String, String>,
) {
    val attributes = attributes.toMutableMap()

    companion object {
        fun root(
            name: String,
            attributes: Map<String, String>,
        ): XMLTag = XMLTag(null, name, attributes)
    }
    val parent: XMLTag = parentRef ?: this
    val children = mutableListOf<XMLTag>()

    fun tag(name: String, attributes: Map<String, String>): XMLTag {
        return XMLTag(this, name, attributes).also {
            children.add(it)
        }
    }

    val isRoot = parent == this

    private val depth: Int
        get() {
            return if (this == parent) {
                0
            } else {
                this.parent.depth + 1
            }
        }

    override fun toString(): String {
        val indent = "    ".repeat(depth)
        return "$indent$name: ${attributes.entries.joinToString { "${it.key}=${it.value}" }}"
    }

//    fun dimensionedValue(key: String): DimensionedValue {
//        return attributes[key]?.let {
//            return DimensionedValue.from(it)
//        } ?: DimensionedValue.none
//    }

    fun addText(string: String) {
        attributes["text"] = (attributes["text"] ?: "") + string
    }

    fun flatten(list: MutableList<XMLTag>) {
        list.add(this)
        children.forEach { it.flatten(list) }
    }

    val flattened get() = mutableListOf<XMLTag>()
        .also {
            flatten(it)
        }
        .toList()
}
