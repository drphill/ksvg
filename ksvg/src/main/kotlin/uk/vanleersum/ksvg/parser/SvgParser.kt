package uk.vanleersum.ksvg.parser
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import org.apache.logging.log4j.kotlin.Logging
import org.xml.sax.Attributes
import org.xml.sax.ext.DefaultHandler2

/**
 * Used as a Handler for a SAXParser.  Creates a tree of [XMLTag]s from a xml document.
 */
class SvgParser : DefaultHandler2(), Logging {
    var root: XMLTag? = null
        private set
    private var currentTag: XMLTag? = null

    private fun convert(attributes: Attributes?): Map<String, String> {
        if (null == attributes) return emptyMap()
        return (0 until attributes.length).map {
            attributes.getQName(it) to attributes.getValue(it)
        }.toMap()
    }

    override fun endDocument() {
        logger.debug { "SvgParser.endDocument :" }
    }

    override fun endElement(uri: String, localName: String, qName: String) {
        currentTag = currentTag?.parent
        // logger.debug { "<< : $currentTag" }
    }

    override fun startDocument() {
        logger.debug { "SvgParser.startDocument :" }
    }

    override fun startElement(uri: String, localName: String, qName: String, attributes: Attributes) {
        if (null == root) {
            root = XMLTag.root(qName, convert(attributes))
            currentTag = root
        } else {
            currentTag = currentTag?.tag(qName, convert(attributes))
        }
    }

    override fun comment(ch: CharArray, start: Int, length: Int) {
        logger.debug { "COMMENT" }
        // logger.debug { StringEscapeUtils.unescapeXml(String(ch)) }
    }

    override fun characters(ch: CharArray, start: Int, length: Int) {
        currentTag?.addText(String(ch, start, length))
    }
}
